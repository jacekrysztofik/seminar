#HSLIDE

#### MeRCI - Meter Remote Control Interface

albo

#### Układ zdalnego sterowania<br />optoelektronicznym systemem pomiarowym<br />wybranych wielkości fizycznych

Praca inżynierska pod kierunkiem<br />dra hab. inż. Piotra Kisały, prof. PL.

#VSLIDE

#### Cel i zakres pracy

Celem pracy jest wykazanie możliwości zdalnego sterowania
optoelektronicznym układem do pomiaru wybranych wielkości fizycznych
wykorzystującym czujniki zapisane na włóknach światłowodowych.

#HSLIDE

### Jak to jest zrobione?

![Image-Relative](assets/fbg/grating-electron-microscope.gif) <!-- .element: class="fragment" -->

#VSLIDE

![Image-Relative](assets/fbg/burning1.jpeg)

#HSLIDE

### Jak to działa?

![Image-Relative](assets/fbg/fbg-operation-principle.jpg)

#VSLIDE

### Jak to działa?

![Image-Relative](assets/fbg/techno-bragg-e.jpg)

#VSLIDE

### Jak to działa?

![Image-Relative](assets/fbg/strain-sensor-2.png) <br />
![Image-Relative](assets/fbg/chemical-sensor.jpg) <!-- .element: class="fragment" -->

#HSLIDE

### Układ pomiarowy

![Image-Relative](assets/thorlabs/optics-system.png)

#VSLIDE

### Źródło sygnału

![Image-Relative](assets/genius/warning.png)

#VSLIDE

### Fotodetektor

![Image-Relative](assets/thorlabs/photodetector.png)

#VSLIDE

### Miernik

![Image-Relative](assets/thorlabs/pm320e.png)

#VSLIDE

### Komunikacja

#### TMC: Test and Measurement Class

- IEEE-488: General Purpose Interface Bus (1975) <!-- .element: class="fragment" -->
- IEEE-488.2: Skippy - Standard Commands for Programmable Instruments <!-- .element: class="fragment" -->

#### National Instruments <!-- .element: class="fragment" -->

#VSLIDE

### Komunikacja

#### Łącza

- GPIB <!-- .element: class="fragment" -->
- USB <!-- .element: class="fragment" -->
- Ethernet <!-- .element: class="fragment" -->
- RS-232 <!-- .element: class="fragment" -->
- PXI - PCI eXtensions for Instrumentation <!-- .element: class="fragment" -->
- VXI - VMEbus eXtensions for Instrumentation <!-- .element: class="fragment" -->

#HSLIDE

### PM320E

![Image-Relative](assets/thorlabs/pm320e.png)

#VSLIDE

### PM320E

- 2 kanały <!-- .element: class="fragment" -->
- USB <!-- .element: class="fragment" -->
- C++, C# <!-- .element: class="fragment" -->
- VISA <!-- .element: class="fragment" -->
- LabView <!-- .element: class="fragment" -->
- Windows <!-- .element: class="fragment" -->

#VSLIDE?image=https://gitlab.com/jacekrysztofik/seminar/raw/master/assets/linux/12990-green-hills-behind-broken-window-1920x1080-digital-art-wallpaper.jpg

#### Windows jako metoda utrudniania pracy

- cena licencji systemu <!-- .element: class="fragment" -->
- cena licencji LabView <!-- .element: class="fragment" -->
- pamięciożerny <!-- .element: class="fragment" -->
- zamknięty kod sterowników <!-- .element: class="fragment" -->
- można zapomnieć o minituryzacji <!-- .element: class="fragment" -->

#VSLIDE

![Image-Relative](assets/thorlabs/eureka.png)
### IEEE-488.2 = SCPI <!-- .element: class="fragment" -->

#VSLIDE

![Image-Relative](assets/linux/gnu480.png)
![Image-Relative](assets/linux/tux480.png)
![Image-Relative](assets/linux/gnutux480.png)

#### GNU/Linux jako narzędzie pracy

- otwarty kod <!-- .element: class="fragment" -->
- możliwość łatwego okrojenia systemu <!-- .element: class="fragment" -->
- widać, jak napisany jest sterownik <!-- .element: class="fragment" -->
- czasem wystarczy dłużej poszukać <!-- .element: class="fragment" -->

#VSLIDE

### USB-TMC

![Image-Relative](assets/linux/cern1.png) <!-- .element: class="fragment" -->
![Image-Relative](assets/linux/cern2.png) <!-- .element: class="fragment" -->

#VSLIDE

![Image-Relative](assets/troll/angry.png)
## czasem wystarczy dłużej poszukać <!-- .element: class="fragment" -->

#VSLIDE

![Image-Relative](assets/linux/pythonpm100d.png)

#VSLIDE

![Image-Relative](assets/thorlabs/pm100d.png)

#VSLIDE

![Image-Relative](assets/thorlabs/meter-sensor.png)

#VSLIDE

![Image-Relative](assets/troll/happy.png)

#VSLIDE

### Python

- python-usbtmc <!-- .element: class="fragment" -->
- python-ivi <!-- .element: class="fragment" -->
- PyVISA <!-- .element: class="fragment" -->

#HSLIDE

### Raspberry Pi

![Image-Relative](assets/raspberry/pi2hand.png) <!-- .element: class="fragment" -->
![Image-Relative](assets/raspberry/b+pinout.png) <!-- .element: class="fragment" -->

#VSLIDE

### Nano Pi

![Image-Relative](assets/raspberry/nanopi1.png) <!-- .element: class="fragment" -->
![Image-Relative](assets/raspberry/nanopi2.png) <!-- .element: class="fragment" -->
![Image-Relative](assets/raspberry/nanopinout.png) <!-- .element: class="fragment" -->

#HSLIDE

### USB/IP

- podłączanie do surowych urządzeń USB przez TCP/IP <!-- .element: class="fragment" -->

### Zabezpieczenia <!-- .element: class="fragment" -->

- firewall (TCP:3240) <!-- .element: class="fragment" -->
- VPN - bardziej ogólne rozwiązanie <!-- .element: class="fragment" -->
- uprawnienia do urządzień lokalnych <!-- .element: class="fragment" -->

#VSLIDE

### UDEV

![Image-Relative](assets/code/udev.png) <!-- .element: class="fragment" -->
![Image-Relative](assets/code/udev-result.png) <!-- .element: class="fragment" -->

#VSLIDE

![Image-Relative](assets/code/usbip_local.png)

#VSLIDE

![Image-Relative](assets/code/usbip_remote.png)

#HSLIDE

### WEBSERVICE

- REST znakomicie pasuje do obsługi miernika <!-- .element: class="fragment" -->
- ustawianie korekcji <!-- .element: class="fragment" -->
- odczyt stanu <!-- .element: class="fragment" -->
- dane w HTML, XML, JSON <!-- .element: class="fragment" -->

#VSLIDE

![Image-Relative](assets/code/fcgi_apache.png)

#VSLIDE

![Image-Relative](assets/code/fcgi_0.png)

#VSLIDE

![Image-Relative](assets/code/fcgi_1.png)

#VSLIDE

![Image-Relative](assets/code/web.png)
<a href="http://swidnik/pm100d/meter/set.fcgi" target="_blank">Live show</a> <!-- .element: class="fragment" -->

#HSLIDE

### Dystrybucja oprogramowania

- własny kod <!-- .element: class="fragment" -->
- ustawienia systemowe (udev, apache/nginx) <!-- .element: class="fragment" -->
- zależności (wymagane biblioteki i usługi) <!-- .element: class="fragment" -->
- aktualizacja <!-- .element: class="fragment" -->
- python-usbtmc <!-- .element: class="fragment" -->
- pakiety <!-- .element: class="fragment" --> <b><i>.deb</i></b> <!-- .element: class="fragment" -->
![Image-Relative](assets/code/python-debianize.png) <!-- .element: class="fragment" -->

#VSLIDE

![Image-Relative](assets/code/python_buildpackage_command.png)

#VSLIDE

![Image-Relative](assets/code/python-buildpackage_result.png)

#HSLIDE

### Warsztat

![Image-Relative](assets/home/locker1.jpg)

#VSLIDE

![Image-Relative](assets/home/locker2.jpg)

#VSLIDE

#### Prototyp prototypu.

![Image-Relative](assets/home/2.jpg) <!-- .element: class="fragment" -->
![Image-Relative](assets/home/3.jpg) <!-- .element: class="fragment" -->

#VSLIDE

### Laboratorium

![Image-Relative](assets/home/table1.jpg)

#VSLIDE

![Image-Relative](assets/home/table2.jpg)

#VSLIDE

![Image-Relative](assets/home/vesa.jpg)

#VSLIDE

![Image-Relative](assets/genius/laser.jpg)

#VSLIDE

![Image-Relative](assets/genius/popcorn.jpg)

#HSLIDE

![Image-Relative](assets/genius/aced.jpg)

